set -x CXX clang++
set -x CC clang
set -x ANDROID_HOME /home/topi/Android/Sdk

set -x fish_user_paths $ANDROID_HOME/emulator $fish_user_paths
set -x fish_user_paths $ANDROID_HOME/platform-tools $fish_user_paths
set -x fish_user_paths ~/.cargo/bin/ $fish_user_paths
set -x fish_user_paths ~/.local/bin/ $fish_user_paths
set -x fish_user_paths ~/.bun/bin/ $fish_user_paths
set -x fish_user_paths ~/go/bin/ $fish_user_paths


set -x LD_LIBRARY_PATH /usr/local/lib/
set -x DEBUGINFOD_URLS "https://debuginfod.archlinux.org"
set -x EDITOR nvim
set -x GTK_THEME Adwaita:dark
set fish_greeting

if status --is-login
    if test -z "$DISPLAY" -a (tty) = /dev/tty1
	XDG_CURRENT_DESKTOP=Hyprland dbus-run-session Hyprland
    end
end  
