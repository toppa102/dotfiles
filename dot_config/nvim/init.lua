require("mappings")
require("plugins")
require("setup_plugins")
require("lualine_setup")
require("lsp")
require("snip_and_cmp")

vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.pumheight = 20

vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.showmode = false
